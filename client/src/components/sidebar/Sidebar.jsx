import "./sidebar.scss";
import DashboardIcon from '@mui/icons-material/Dashboard';
import ExitToAppIcon from "@mui/icons-material/ExitToApp";
import AccountCircleOutlinedIcon from "@mui/icons-material/AccountCircleOutlined";
import TimeToLeaveIcon from '@mui/icons-material/TimeToLeave';
import { Link } from "react-router-dom";
import { signOut } from "firebase/auth";
import { useContext } from "react";
import { AuthContext } from "../../context/AuthContext";
import { auth } from "../../firebase";



const Sidebar = () => {
    const { dispatch } = useContext(AuthContext);
    const handleLogout = () => {
        signOut(auth).then(() => {
            dispatch({ type: "LOGOUT" });
        }).catch((error) => {
            // An error happened.
        });
    }

    return (
        <div className="sidebar">
            <div className="top">
                <span className="logo">
                    ini logo
                </span>
            </div>
            <hr />
            <div className="center">
                <ul>
                    <p className="title">MAIN</p>
                    <Link to="/dashboard" style={{ textDecoration: "none" }}>
                        <li>
                            <DashboardIcon className="icon" />
                            <span>Dashboard</span>
                        </li>
                    </Link>
                    <p className="title">LISTS</p>
                    <Link to="/cars" style={{ textDecoration: "none" }}>
                        <li>
                            <TimeToLeaveIcon className="icon" />
                            <span>Cars</span>
                        </li>
                    </Link>
                    <p className="title">USER</p>
                    <li>
                        <AccountCircleOutlinedIcon className="icon" />
                        <span>Profile</span>
                    </li>
                    <li onClick={handleLogout}>
                        <ExitToAppIcon className="icon" />
                        <span>Logout</span>
                    </li>
                </ul>
            </div>
        </div>
    )
}

export default Sidebar
