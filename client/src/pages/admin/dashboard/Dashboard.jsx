import React, { useContext } from 'react'
import Navbar from '../../../components/navbar/Navbar'
import Sidebar from '../../../components/sidebar/Sidebar'
import TableOrder from '../../../components/tableorder/TableOrder'
import "./dashboard.scss"

import {
    XAxis,
    CartesianGrid,
    Tooltip,
    YAxis,
    BarChart, Bar, Legend,
} from "recharts";

import { Data } from "../../../App"

const Dashboard = () => {
    const { data } = useContext(Data)
    console.log(data)
    const transformCarsType = (arr) => {
        let temp = []
        for (const i of arr) {
            temp.push(i.type)
        }
        temp = temp.reduce((acc, curr) => ((acc[curr] = (acc[curr] || 0) + 1), acc), {})
        return Object.keys(temp).map((key) => ({ type: key, count: temp[key] }))
    }
    const count = (transformCarsType(data))
    console.log(count)
    const dataCar = [
        { name: "Besar", Total: count[1] ? count[1].count : 0 },
        { name: "Sedang", Total: count[0] ? count[0].count : 0 },
        { name: "Kecil", Total: count[2] ? count[2].count : 0 },
    ];
    return (
        <div className='dashboard'>
            <Sidebar />
            <div className="dashboard-container">
                <Navbar />
                <div className="dashboard-main">
                    <p> <b>Dashboard {'>'} </b> Dashboard</p>
                    <h3>Dashboard</h3>
                    <BarChart width={1000} height={400} data={dataCar}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis dataKey="Total" />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="Total" fill="#8884d8" />
                    </BarChart>
                    <TableOrder />
                </div>
            </div>
        </div>
    )
}

export default Dashboard
