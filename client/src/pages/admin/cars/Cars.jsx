import "./cars.scss"
import Sidebar from "../../../components/sidebar/Sidebar";
import Navbar from "../../../components/navbar/Navbar";
import Card from "../../../components/card/Card";
import AddIcon from '@mui/icons-material/Add';
import { Link } from "react-router-dom";
import { useContext, useEffect } from "react";
import { collection, onSnapshot } from "firebase/firestore";
import { db } from "../../../firebase";
import { Data } from "../../../App";


const Cars = () => {
    const { data, setData } = useContext(Data)

    useEffect(() => {
        const unsub = onSnapshot(
            collection(db, "cars"),
            (snapShot) => {
                let list = [];
                snapShot.docs.forEach((doc) => {
                    list.push({ id: doc.id, ...doc.data() });
                });
                setData(list);
            },
            (error) => {
                console.log(error);
            }
        );

        return () => {
            unsub();
        };
    }, [setData]);

    return (
        <div className="cars">
            <Sidebar />
            <div className="carsContainer">
                <Navbar />
                <div className="carsMain">
                    <p> <b>Cars {'>'} </b> List Cars</p>
                    <div className="addButton">
                        <h3>List Cars</h3>
                        <Link to="/cars/new" className="link">
                            <AddIcon />
                            Add New Car
                        </Link>
                    </div>
                    {
                        // isLoading ? (
                        //     <>
                        //         <div className="shimmer">
                        //             <ShimmerSimpleGallery card caption imageHeight={350} col={4} row={2} />
                        //         </div>
                        //     </>
                        // ) : (
                        data.length === 0 ? (
                            <div className="cardContainer">
                                <h1>No Data</h1>
                            </div>
                        ) : (
                            <div className="cardContainer">
                                {
                                    data.map(car => (
                                        <Card key={car.id} id={car.id} />

                                    ))
                                }
                            </div>
                        )
                        // )
                    }
                </div>
            </div>
        </div >
    )
}

export default Cars
