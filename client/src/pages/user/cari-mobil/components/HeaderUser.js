import React from 'react'
import { Box } from '@mui/material';
import HeroUser from './HeroUser';
import NavUser from './NavUser';

const HeaderUser = () => {
    return (
        <Box
            bgcolor="secondary.main"
            component="header"
            position={'relative'}
            px={12}
            mb={12}
        >
            <NavUser />
            <HeroUser />
        </Box >
    )
}

export default HeaderUser
