import React from 'react'
import { Button, FormControl, Grid, MenuItem, TextField, Typography } from '@mui/material'

const Search = () => {
    return (
        <Grid
            position={'absolute'}
            container
            top={'60%'}
            width={'80%'}
            pb={'2rem'}
            pr={'2rem'}

            bgcolor="white"
            sx={{
                left: '50%',
                transform: 'translate(-50%, -50%)',
                boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
                borderRadius: '10px',

            }}
            spacing={2}
            display='flex'
            justifyContent='center'
            zIndex={'100'}
        >
            <Grid item xs={3}>
                <FormControl fullWidth>
                    <Typography paragraph>Tipe Mesin</Typography>
                    <TextField
                        select
                        defaultValue='semua'

                    >
                        <MenuItem disabled value="">
                            <em>Pilih Jenis Mesin</em>
                        </MenuItem>
                        <MenuItem value={'semua'}>Semua</MenuItem>

                    </TextField>
                </FormControl>
            </Grid>

            <Grid item xs={3}>
                <FormControl fullWidth>
                    <Typography paragraph>Tipe Mobil</Typography>
                    <TextField
                        select

                        defaultValue='semua'
                    >
                        <MenuItem disabled value="">
                            <em>Pilih Tipe Mobil</em>
                        </MenuItem>

                        <MenuItem value={'semua'}>Semua</MenuItem>

                    </TextField>

                </FormControl>

            </Grid>

            <Grid item xs={3}>
                <FormControl fullWidth>
                    <Typography paragraph>Tahun Mobil</Typography>
                    <TextField
                        select
                        defaultValue='semua'

                    >
                        <MenuItem disabled value="">
                            <em>Pilih Harga Mobil</em>
                        </MenuItem>

                        <MenuItem value={'semua'}>Semua</MenuItem>

                    </TextField>
                </FormControl>

            </Grid>

            <Grid item xs={3} display={'flex'}>
                <Grid item xs={10}>
                    <FormControl >
                        <Typography paragraph>Jumlah Penumpang (optional)</Typography>
                        <TextField
                            select
                            defaultValue='semua'

                        >
                            <MenuItem disabled value="">
                                <em>Pilih Tipe Mobil</em>
                            </MenuItem>

                            <MenuItem value={'semua'}>Semua</MenuItem>

                        </TextField>

                    </FormControl>
                </Grid>
                <Grid item xs={5}
                    position={'relative'}
                    justifyContent={'center'}
                    alignItems={'center'}
                >
                    <Button
                        variant="contained"
                        color="success"
                        fullWidth
                        sx={{ position: 'absolute', bottom: '0', height: '3.5rem' }}
                    >
                        <Typography color={'white'} sx={{ fontWeight: 'bold', fontSize: '0.9rem' }}
                        >Cari Mobil</Typography>
                    </Button>

                </Grid>
            </Grid>
        </Grid>
    )
}

export default Search
