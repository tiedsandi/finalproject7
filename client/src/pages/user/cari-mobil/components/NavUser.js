import React from 'react'
import { AppBar, Box, Typography, Avatar } from '@mui/material'
// import Logo from '../Logo'

// UseContext
// import { Data } from '../../App'

const NavUser = () => {
    // const { user } = React.useContext(Data)

    const logout = () => {
        window.open("http://localhost:5000/auth/logout", "_self")
    }
    return (
        <AppBar position="static"
            style={{
                background: 'transparent',
                boxShadow: 'none',
                padding: '1rem 0 8rem',
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}
            >
                {/* <Logo /> */}
                <Box
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        gap: '10px',
                    }}
                >
                    {/* <Avatar src={user.hasOwnProperty("photos") ? user.photos[0].value : ""} />
                    <Typography
                        color="textPrimary"
                    >
                        {user.displayName || user.username}
                    </Typography> */}
                    <Typography
                        variant="button"
                        color="black"
                        onClick={logout}
                        cursor="pointer"
                    >
                        Logout
                    </Typography>
                </Box>
            </Box>
        </AppBar >
    )
}
export default NavUser

