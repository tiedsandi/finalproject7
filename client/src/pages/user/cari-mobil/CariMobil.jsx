import "./carimobil.sass"

import HeaderUser from "./components/HeaderUser";
import Search from "./components/Search"
import Footer from "../../main/home/container/Footer"
const CariMobil = () => {

    return (
        <div className="carimobil">
            <HeaderUser />
            <Search />
            <Footer />
        </div>
    )
}

export default CariMobil
