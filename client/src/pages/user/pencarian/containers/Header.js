import React from 'react'
import { Box } from '@mui/material';
import NavUser from '../../cari-mobil/containers/NavUser';

const HeaderUser = () => {
    return (
        <Box
            bgcolor="secondary.main"
            component="header"
            position={'relative'}
            px={12}
            mb={12}
        >
            <NavUser />
        </Box >
    )
}

export default HeaderUser
