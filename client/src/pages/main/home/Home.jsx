import React from 'react'
import "./home.scss"

import HeaderHome from './container/HeaderHome'
import OurServices from './container/OurServices'
import Footer from './container/Footer'

const Home = () => {
    return (
        <>
            <HeaderHome />
            <OurServices />
            <Footer />
        </>
    )
}

export default Home
