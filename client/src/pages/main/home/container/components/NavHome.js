import React from 'react'
import {
    AppBar, Box, Typography, Link, Button,
    // Avatar 
} from '@mui/material'
import Logo from './Logo'
import NavLink from './NavLink'

// UseContext
// import { Data } from '../../App'

const NavHome = () => {
    // const { user } = React.useContext(Data)

    // const logout = () => {
    //     window.open("http://localhost:5000/auth/logout", "_self")
    // }
    return (
        <AppBar position="static"
            style={{
                background: 'transparent',
                boxShadow: 'none',
                padding: '1rem 0 8rem',
            }}
        >
            <Box
                sx={{
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'space-between',
                }}
            >
                <Logo />
                <Box
                    sx={{
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'space-between',
                        gap: '10px',
                    }}
                >
                    {/* {user ? (
                        <>
                            <NavLink />
                            <Avatar src={user.hasOwnProperty("photos") ? user.photos[0].value : ""} />
                            <Typography
                                color="textPrimary"
                            >
                                {user.displayName || user.username}
                            </Typography>
                            <Typography
                                variant="button"
                                color="black"
                                onClick={logout}
                                cursor="pointer"
                            >
                                Logout
                            </Typography>
                        </>

                    ) : (
                        <> */}
                    <NavLink />
                    <Button
                        component={Link}
                        href="/login"
                        variant="contained"
                        color="success">
                        <Typography
                            variant="button"
                            color="white"
                        >
                            Login
                        </Typography>
                    </Button>
                    {/* </>
                    )} */}
                </Box>
            </Box>
        </AppBar >
    )
}
export default NavHome

