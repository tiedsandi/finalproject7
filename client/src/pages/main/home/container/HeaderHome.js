import React from 'react'
import { Box } from '@mui/material';

import HeroHome from './components/HeroHome'
import NavHome from './components/NavHome'

const HeaderHome = () => {
    return (
        <Box
            bgcolor="secondary.main"
            component="header"
            position={'relative'}
            px={12}
            mb={12}
        >
            <NavHome />
            <HeroHome />
        </Box >
    )
}

export default HeaderHome
