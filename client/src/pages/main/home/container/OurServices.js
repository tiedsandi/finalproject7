import { Grid, Typography } from '@mui/material'
import { Box } from '@mui/system'
import React from 'react'

const OurServices = () => {
    return (
        <Grid container spacing={0}>
            <Grid item md={12} lg={6}>
                {/* add image */}
                <Box sx={{
                    ml: { xs: 25 },
                }} >
                    <img src="./images/img_service.png" alt="" />
                </Box>
            </Grid>
            <Grid item xs={6}>
                <Typography variant="h3">
                    Best Car Rental for any kind of trip in (Lokasimu)!
                </Typography>
                <Typography paragraph>
                    Sewa mobil di (Lokasimu) bersama Binar Car Rental jaminan harga lebih murah
                    dibandingkan yang lain, kondisi mobil baru, serta kualitas pelayanan terbaik untuk perjalanan
                    wisata, bisnis, wedding, meeting, dll.
                </Typography>
            </Grid>
        </Grid>
    )
}

export default OurServices
