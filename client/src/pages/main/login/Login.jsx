import {
    useContext,
    useEffect, useState
} from 'react';
import { TextField, Button, Link } from '@mui/material';
import { Stack, Avatar, Box, Grid, Divider } from '@mui/material';
import { signInWithEmailAndPassword } from "firebase/auth";
import { auth } from '../../../firebase';
import { useNavigate } from 'react-router-dom';
import { AuthContext } from '../../../context/AuthContext';

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [error, setError] = useState(false);
    // console.log(email, password);

    const navigate = useNavigate();

    const { dispatch } = useContext(AuthContext);

    const handleLogin = async (e) => {
        e.preventDefault();
        signInWithEmailAndPassword(auth, email, password)
            .then((userCredential) => {
                const user = userCredential.user;
                dispatch({ type: "LOGIN", payload: user })
                navigate('/dashboard');
            })
            .catch((error) => {
                setError(true)
            });
    };

    useEffect(() => {

    }, [])


    return (
        <Grid container >
            <Grid item xs={8}>
                <Box
                    display="flex"
                    flexDirection="column"
                    style={{
                        backgroundImage: 'url(/images/login.png)',
                        backgroundSize: 'cover',
                        backgroundRepeat: 'no-repeat',
                        backgroundPosition: 'center',
                        height: '100vh',
                        zIndex: -1,
                        // backgroundcolor and opacity
                        backgroundColor: 'rgba(13, 40, 166, 0.6)',
                    }}
                />
            </Grid>
            <Grid item xs={4}>
                <Box
                    display="flex"
                    flexDirection="column"
                    style={{
                        backgroundColor: '#fff',
                        height: '100vh',
                        padding: '20px',
                        borderRadius: '10px',
                        boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
                        zIndex: 1,
                        justifyContent: 'center',
                        alignItems: 'center',
                    }}
                >
                    <Box
                        display="flex-start"
                        flexDirection="column"
                        style={{
                            fontSize: '30px',
                            fontWeight: 'bold',
                            color: '#000',
                            marginBottom: '20px',
                        }}
                    >
                        Login
                    </Box>
                    <form action="" onSubmit={handleLogin}
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            padding: '3rem',
                            boxShadow: '0px 0px 10px rgba(0, 0, 0, 0.1)',
                            marginBottom: '20px',
                            zIndex: 1,
                        }}
                    >
                        {error && <p style={{ color: 'red' }}>Invalid email or password</p>}
                        <TextField
                            id="email-input"
                            name="email"
                            label="Email"
                            type="email"
                            onChange={(e) => setEmail(e.target.value)}
                            style={{
                                marginBottom: '20px',
                            }}
                        />
                        <TextField
                            id="password-input"
                            name="password"
                            label="Password"
                            type="password"
                            onChange={(e) => setPassword(e.target.value)}
                            style={{
                                marginBottom: '20px',
                            }}
                        />
                        <Button variant="contained" color="primary" type="submit">
                            Submit
                        </Button>
                    </form>
                    <Stack
                        direction="row"
                        divider={<Divider orientation="vertical" flexItem />}
                        spacing={2}
                        style={{
                            justifyContent: 'center',
                            alignItems: 'center',
                        }}
                    >
                        <Link
                            // onClick={google} 
                            variant="body2" sx={{ cursor: 'pointer' }}>
                            <Avatar
                                src="/images/gmail.png"
                                sx={{ width: 56, height: 56, }}
                            />
                        </Link>
                        <Link
                            // onClick={github} 
                            variant="body2" sx={{ cursor: 'pointer' }}>
                            <Avatar
                                src="/images/github.png"
                                sx={{ width: 56, height: 56 }} />
                        </Link>
                        <Link
                            // onClick={facebook} 
                            variant="body2" sx={{ cursor: 'pointer' }}>
                            <Avatar
                                src="/images/facebook.png"
                                sx={{ width: 56, height: 56 }} />
                        </Link>
                    </Stack>
                </Box>
            </Grid>
        </Grid>
    )
}

export default Login
