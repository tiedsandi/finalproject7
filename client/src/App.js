import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";
import { createContext, useContext, useState } from 'react';

import Home from "./pages/main/home/Home"
import Login from "./pages/main/login/Login"

import Dashboard from "./pages/admin/dashboard/Dashboard";
import Cars from "./pages/admin/cars/Cars";
import EditCar from "./pages/admin/cars/edit/EditCar";
import NewCar from "./pages/admin/cars/new/NewCar";
import CariMobil from "./pages/user/cari-mobil/CariMobil";

import { ThemeProvider } from '@mui/material'
import { Theme } from './Theme';
import { AuthContext } from "./context/AuthContext";


export const Data = createContext();

function App() {
  const [user, setUser] = useState(null);
  const [data, setData] = useState([]);

  const { currentUser } = useContext(AuthContext)

  const RequireAuth = ({ children }) => {
    return currentUser ? (children) : <Navigate to="/login" />;
  };

  return (
    <ThemeProvider theme={Theme}>
      <Data.Provider value={{ user, setUser, data, setData }}>
        <div className="App">
          <Router>
            <Routes>
              <Route path="/">
                <Route index element={<Home />} />
                <Route path="login" element={<Login />} />


                {/* Admin */}
                <Route path="dashboard" element={
                  <RequireAuth>
                    <Dashboard />
                  </RequireAuth>
                } />
                <Route path="cars">
                  <Route index element={
                    <RequireAuth>
                      <Cars />
                    </RequireAuth>}
                  />
                  <Route path="edit/:id" element={
                    <RequireAuth>
                      <EditCar />
                    </RequireAuth>}
                  />
                  <Route path="new" element={
                    <RequireAuth>
                      <NewCar />
                    </RequireAuth>
                  } />
                </Route>

                {/* user */}
                <Route path="cari-mobil">
                  <Route index element={
                    <RequireAuth>
                      <CariMobil />
                    </RequireAuth>
                  } />
                  {/* <Route path=":productId" element={<MobilUser />} /> */}
                </Route>
              </Route>
            </Routes>
          </Router>
        </div>
      </Data.Provider>
    </ThemeProvider >
  );
}

export default App;
