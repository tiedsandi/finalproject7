import apiBinar from "../apiBinar";

const getAll = () => {
    return apiBinar.get("/mobil");
}

const getById = (id) => {
    return apiBinar.get(`/mobil/${id}`);
}

const create = (data) => {
    return apiBinar.post("/mobil", data);
}

const update = (id, data) => {
    return apiBinar.put(`/mobil/${id}`, data);
}

const remove = (id) => {
    return apiBinar.delete(`/mobil/${id}`);
}

const BinarService = {
    getAll,
    getById,
    create,
    update,
    remove
}

export default BinarService;